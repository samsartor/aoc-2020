use anyhow::{anyhow, bail, Context, Error};
use select::{document::Document, predicate::Name};
use std::fs::{read_to_string, write};
use std::path::PathBuf;
use std::process::Command;
use std::thread::spawn;
use ureq::Response;

pub fn request(url: &str, session: &str) -> Response {
    ureq::get(url)
        .set("Cookie", &format!("session={}", session.trim()))
        .call()
}

pub fn download_input(day: usize, session: &str) -> Result<bool, Error> {
    let input_path = PathBuf::from(format!("inputs/day{:02}.txt", day));
    if input_path.exists() {
        return Ok(false);
    }

    let res = request(
        &format!("http://adventofcode.com/2020/day/{}/input", day),
        session,
    );

    if res.status() != 200 {
        bail!(
            "AoC server refused to download input for day {}:\n{}",
            day,
            res.into_string()?
        );
    }

    let input = res.into_string()?;
    write(&input_path, &input)?;
    Ok(true)
}

fn find_example(doc: &Document) -> Option<String> {
    let part1 = doc.find(Name("article")).next()?;

    let mut next_example = false;
    for node in part1.children() {
        if next_example {
            if let Some(code) = node.find(Name("code")).next() {
                return Some(code.text());
            }
        }

        if node.is(Name("p")) {
            next_example = node.text().contains("For example");
        }
    }

    None
}

pub fn pandoc_into(input_path: &str, output_path: &str, format: &str) -> Result<(), Error> {
    let status = Command::new("pandoc")
        .arg(input_path)
        .arg("--from")
        .arg("html")
        .arg("--to")
        .arg(format)
        .arg("--output")
        .arg(output_path)
        .status()
        .context("could not run pandoc")?;
    if status.success() {
        Ok(())
    } else {
        Err(anyhow!("pandoc failed to convert problem"))
    }
}

pub fn download_problem(day: usize, session: &str) -> Result<Document, Error> {
    let res = request(
        &format!("http://adventofcode.com/2020/day/{}", day),
        session,
    );

    if res.status() != 200 {
        bail!(
            "AoC server refused to download problem for day {}:\n{}",
            day,
            res.into_string()?
        );
    }

    Ok(Document::from_read(res.into_reader())?)
}

#[derive(argh::FromArgs)]
/// Download inputs and problems for advent of code 2020.
struct AdventOfCodeDowload {
    /// the day number
    #[argh(positional)]
    day: usize,
}

fn main() -> Result<(), Error> {
    let AdventOfCodeDowload { day } = argh::from_env::<AdventOfCodeDowload>();
    let session = read_to_string("session").context("./session not found")?;

    let input_session = session.clone();
    let input_dl = spawn::<_, Result<(), Error>>(move || {
        if download_input(day, &input_session)? {
            println!("Downloaded input for day {}.", day)
        }
        Ok(())
    });

    let problem_session = session.clone();
    let problem_dl = spawn::<_, Result<(), Error>>(move || {
        let html = download_problem(day, &problem_session)?;
        let content: String = html
            .find(Name("article"))
            .map(|node| node.inner_html())
            .collect();

        let html_path = format!("problems/day{:02}.html", day);
        write(&html_path, &content)?;
        println!("Downloaded problem for day {}.", day);
        let _ = opener::open(&html_path);

        let pandoc_res = pandoc_into(
            &html_path,
            &format!("problems/day{:02}.md", day),
            "markdown",
        );
        if let Err(e) = pandoc_res.as_ref() {
            println!("WARNING: Markdown conversion failed.\n{}", e);
        }

        if let Some(example) = find_example(&html) {
            write(format!("inputs/day{:02}_example.txt", day), &example)?;
            println!("Downloaded example problem for day {}.", day);
        }

        Ok(())
    });

    if let Ok(Err(e)) = input_dl.join() {
        println!("WARNING: Could not download input.\n{}", e);
    }

    if let Ok(Err(e)) = problem_dl.join() {
        println!("WARNING: Could not download problem.\n{}", e);
    }

    Ok(())
}
