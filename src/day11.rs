use crate::*;

impl Grid {
    fn can_see_occupied(&self, x: isize, y: isize, dx: isize, dy: isize) -> bool {
        for c in self.cast(x, y, dx, dy).skip(1) {
            match c {
                '#' => return true,
                'L' => return false,
                _ => (),
            }
        }

        false
    }

    fn simulate(
        &mut self,
        min_leave: usize,
        num_occupied: impl Fn(&Self, isize, isize) -> usize,
    ) -> usize {
        let mut next = self.clone();
        loop {
            let mut changed = 0;
            for (x, y) in self.coords() {
                let n = num_occupied(self, x, y);
                let value = match self.ati(x, y) {
                    'L' if n == 0 => '#',
                    '#' if n >= min_leave => 'L',
                    _ => continue,
                };
                next.set(x, y, value);
                changed += 1;
            }

            if changed == 0 {
                break;
            }

            self.clone_from(&next);
        }

        self.iter().count_eq('#')
    }
}

pub fn part1(mut grid: Grid) -> usize {
    grid.simulate(4, |g, x, y| g.neighbors(x, y).count_eq('#'))
}

pub fn part2(mut grid: Grid) -> usize {
    grid.simulate(5, |g, x, y| {
        Grid::DIRECTIONS
            .iter()
            .filter(|&&(dx, dy)| g.can_see_occupied(x, y, dx, dy))
            .count()
    })
}
