pub type InputRes<'s, T> = Result<T, (&'s str, &'static str)>;

pub trait Input<'s>: Sized {
    fn read(input: &'s str) -> InputRes<'s, Self>;
}

impl<'s> Input<'s> for char {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        input.parse().map_err(|_| (input, "<char>"))
    }
}

impl<'s> Input<'s> for u32 {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        input.parse().map_err(|_| (input, "<u32>"))
    }
}

impl<'s> Input<'s> for u64 {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        input.parse().map_err(|_| (input, "<u64>"))
    }
}

impl<'s> Input<'s> for usize {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        input.parse().map_err(|_| (input, "<usize>"))
    }
}

impl<'s> Input<'s> for isize {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        input.parse().map_err(|_| (input, "<isize>"))
    }
}

impl<'s> Input<'s> for String {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        Ok(input.to_owned())
    }
}

impl<'s> Input<'s> for &'s str {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        Ok(input)
    }
}

impl<'s> Input<'s> for Box<[u8]> {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        Ok(input.as_bytes().to_owned().into_boxed_slice())
    }
}

impl<'s> Input<'s> for &'s [u8] {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        Ok(input.as_bytes())
    }
}

impl<'s, T: Input<'s>> Input<'s> for Vec<T> {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        input.lines().map(|s| T::read(s)).collect()
    }
}

impl<'s, T: Input<'s>> Input<'s> for InputRes<'s, T> {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        Ok(T::read(input))
    }
}

impl<'s> Input<'s> for ! {
    fn read(_: &str) -> InputRes<'s, Self> {
        todo!()
    }
}

pub fn unwrap_input_res<T>(res: InputRes<T>) -> T {
    match res {
        Ok(x) => x,
        Err((found, expected)) => panic!("Expected {:?} but found {:?}.", found, expected),
    }
}

pub fn substr_row_col(all: &str, sub: &str) -> (usize, usize) {
    let loc = sub.as_ptr() as usize - all.as_ptr() as usize;

    let mut col = 0;
    let mut row = 0;
    for (i, c) in all.char_indices() {
        if i == loc {
            return (row, col);
        }

        if c == '\n' {
            col = 0;
            row += 1;
        } else {
            col += 1;
        }
    }

    panic!("not a substring")
}
