use crate::*;

fn is_valid(part: &[isize], mask: RCombList<bool>) -> bool {
    let mut prev = part[0];

    for (i, &keep) in mask.enumerate() {
        let x = part[i + 1];

        if x - prev > 3 {
            return false;
        }

        if keep {
            prev = x;
        }
    }

    part.last().unwrap() - prev <= 3
}

fn sort_with_outlets(chain: &mut Vec<isize>) {
    chain.push(0);
    chain.sort();
    chain.push(chain.last().unwrap() + 3);
}

pub fn part1(mut chain: Vec<isize>) -> isize {
    sort_with_outlets(&mut chain);

    let mut ones = 0;
    let mut threes = 0;
    for [a, b] in chain.array_windows() {
        match b - a {
            1 => ones += 1,
            3 => threes += 1,
            _ => (),
        }
    }

    ones * threes
}

pub fn part2(mut chain: V<isize>) -> usize {
    sort_with_outlets(&mut chain);

    // Find ranges in the entire chain such that in each range any single
    // adapter (except the first and last) might be remvoed without effecting
    // any other ranges.
    let mut ranges = vec![];
    let mut range_start = None;
    for (a_idx, &[a, _b, c]) in chain.array_windows().enumerate() {
        if c - a <= 3 {
            // Adapter `b` might be removed, mark `a` as the first in the range.
            if range_start.is_none() {
                range_start = Some(a_idx);
            }
        } else {
            // Adapter `b` can't be removed.
            if let Some(start) = range_start.take() {
                // It was the last in the range so add the finished range now.
                ranges.push(&chain[start..=(a_idx + 1)]);
            }
        }
    }

    // Start the accumulator at 1. We'll multiply it by k every time we find k
    // ways to arrange some range of adapters.
    let mut arrangements = 1;

    // For each indepenent range of length 2, find all valid possibilites.
    for range in ranges {
        // Iterate through all 2^(n - 2) possibilites.
        arrangements *= [true, false]
            .replaced_combs_with_k(range.len() - 2)
            .filter(|mask| is_valid(range, mask.clone()))
            .count();
    }

    arrangements
}
