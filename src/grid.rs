use crate::{Input, InputRes};
use std::fmt;
use std::ops::{Index, IndexMut};

#[derive(Clone, Eq, PartialEq)]
pub struct Grid {
    pub w: usize,
    pub h: usize,
    bytes: Vec<u8>,
}

impl Grid {
    pub const DIRECTIONS: [(isize, isize); 8] = [
        (-1, -1),
        (0, -1),
        (1, -1),
        (-1, 0),
        (1, 0),
        (-1, 1),
        (0, 1),
        (1, 1),
    ];

    fn from_lines_impl<'s>(lines: impl IntoIterator<Item = &'s str>) -> InputRes<'s, Self> {
        let mut w = None;
        let mut h = 0;
        let mut bytes = Vec::new();
        for line in lines {
            bytes.extend_from_slice(line.as_bytes());

            if *w.get_or_insert(line.len()) != line.len() {
                return Err((line, "<grid-aligned line>"));
            }

            h += 1;
        }

        Ok(Grid {
            w: w.unwrap_or(0),
            h,
            bytes,
        })
    }

    pub fn from_lines<'s>(lines: impl IntoIterator<Item = &'s str>) -> Self {
        crate::unwrap_input_res(Grid::from_lines_impl(lines))
    }

    pub fn ati(&self, x: isize, y: isize) -> char {
        char::from(self[(x, y)])
    }

    pub fn atu(&self, x: usize, y: usize) -> char {
        char::from(self[(x, y)])
    }

    pub fn get(&self, x: isize, y: isize) -> Option<char> {
        if x < 0 || y < 0 || x >= self.w as isize || y >= self.h as isize {
            None
        } else {
            Some(char::from(self[(x as usize, y as usize)]))
        }
    }

    pub fn set(&mut self, x: isize, y: isize, value: char) {
        if x >= 0 && y >= 0 && x < self.w as isize && y < self.h as isize {
            self[(x as isize, y as isize)] = value as u8;
        }
    }

    pub fn neighbors(&self, x: isize, y: isize) -> impl Iterator<Item = char> + '_ {
        Self::DIRECTIONS
            .iter()
            .filter_map(move |&(dx, dy)| self.get(x + dx, y + dy))
    }

    pub fn coords(&self) -> impl Iterator<Item = (isize, isize)> {
        let h = self.h;
        (0..self.w).flat_map(move |x| (0..h).map(move |y| (x as isize, y as isize)))
    }

    pub fn iter(&self) -> impl Iterator<Item = char> + '_ {
        self.coords().map(move |c| self[c].into())
    }

    pub fn cast(
        &self,
        mut x: isize,
        mut y: isize,
        dx: isize,
        dy: isize,
    ) -> impl Iterator<Item = char> + '_ {
        std::iter::from_fn(move || {
            let out = self.get(x, y);
            x += dx;
            y += dy;
            out
        })
    }
}

impl Index<(usize, usize)> for Grid {
    type Output = u8;

    fn index(&self, (x, y): (usize, usize)) -> &u8 {
        assert!(x < self.w);
        assert!(y < self.h);
        &self.bytes[x + y * self.w]
    }
}

impl IndexMut<(usize, usize)> for Grid {
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut u8 {
        assert!(x < self.w);
        assert!(y < self.h);
        &mut self.bytes[x + y * self.w]
    }
}

impl Index<(isize, isize)> for Grid {
    type Output = u8;

    fn index(&self, (x, y): (isize, isize)) -> &u8 {
        &self[(x as usize, y as usize)]
    }
}

impl IndexMut<(isize, isize)> for Grid {
    fn index_mut(&mut self, (x, y): (isize, isize)) -> &mut u8 {
        &mut self[(x as usize, y as usize)]
    }
}

impl<'s> Input<'s> for Grid {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        Self::from_lines_impl(input.lines())
    }
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for y in 0..self.h {
            if y != 0 {
                writeln!(f)?;
            }
            for x in 0..self.w {
                write!(f, "{}", self.atu(x, y))?;
            }
        }

        Ok(())
    }
}

impl fmt::Debug for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut l = f.debug_list();
        for y in 0..self.h {
            let entry: String = (0..self.w).map(|x| self.atu(x, y)).collect();
            l.entry(&entry);
        }
        l.finish()
    }
}
