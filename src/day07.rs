use crate::*;

#[derive(Debug, Input)]
#[input(regex = r"^([a-z ]+) bags contain (.*)$")]
pub struct Rule<'s> {
    color: &'s str,
    contains: Split<Contains<'s>>,
}

#[derive(Debug, Input)]
#[input(split = ", ")]
pub enum Contains<'s> {
    #[input(regex = r"^(\d+) (.+) bags?\.?$")]
    Bags { num: usize, color: &'s str },
    #[input(regex = r"^no other bags\.$")]
    Nothing,
}

pub fn part1(i: Vec<Rule>) -> usize {
    let mut graph: M<&str, Vec<_>> = M::new();

    for rule in i {
        for inner in rule.contains {
            if let Contains::Bags { color, .. } = inner {
                graph.mutate(color).push(rule.color);
            }
        }
    }

    let mut found = S::new();
    let mut queue = vec!["shiny gold"];
    while let Some(next) = queue.pop() {
        if found.insert(next) {
            if let Some(contained_by) = graph.get(next) {
                queue.extend(contained_by);
            }
        }
    }

    found.len() - 1
}

pub fn part2(input: Vec<Rule>) -> usize {
    let mut graph: M<&str, V<_>> = M::new();

    for rule in input {
        for inner in rule.contains {
            if let Contains::Bags { color, num } = inner {
                graph.mutate(rule.color).push((color, num));
            }
        }
    }

    let mut count = 0;
    let mut queue = vec![("shiny gold", 1)];
    while let Some((next, mul)) = queue.pop() {
        count += mul;
        if let Some(contained_by) = graph.get(next) {
            queue.extend(contained_by.iter().map(|&(c, n)| (c, n * mul)));
        }
    }

    count - 1
}
