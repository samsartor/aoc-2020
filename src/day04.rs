use crate::{Groups, Input, InputRes, Split};

#[derive(Debug, Input, Eq, PartialEq)]
#[input(split = r"\s+")]
pub enum Field {
    #[input(regex = r"^byr:(\d+)$")]
    Byr(usize),
    #[input(regex = r"^iyr:(\d+)$")]
    Iyr(usize),
    #[input(regex = r"^eyr:(\d+)$")]
    Eyr(usize),
    #[input(regex = r"^hgt:(\d+)cm$")]
    HgtCm(usize),
    #[input(regex = r"^hgt:(\d+)in$")]
    HgtIn(usize),
    #[input(regex = r"^hcl:#[0-9a-f]{6}$")]
    Hcl,
    #[input(regex = r"^ecl:(?:amb|blu|brn|gry|grn|hzl|oth)$")]
    Ecl,
    #[input(regex = r"^pid:\d{9}$")]
    Pid,
    #[input(regex = r"^cid:.*$")]
    Cid,
}

impl Field {
    fn is_valid(&self) -> bool {
        use Field::*;

        match self {
            Byr(y) => (1920..=2002).contains(y),
            Iyr(y) => (2010..=2020).contains(y),
            Eyr(y) => (2020..=2030).contains(y),
            HgtCm(c) => (150..=193).contains(c),
            HgtIn(i) => (59..=76).contains(i),
            _ => true,
        }
    }
}

pub type InType<'s> = Groups<Split<InputRes<'s, Field>>>;

pub fn part1(input: InType) -> usize {
    input
        .into_iter()
        .filter(|p| match p.len() {
            8 => true,
            7 => !p.contains(&Ok(Field::Cid)),
            _ => false,
        })
        .count()
}

pub fn part2(input: InType) -> usize {
    input
        .into_iter()
        .filter(|p| {
            let mut count = 0;
            for f in &p.0 .0 {
                match f {
                    Ok(Field::Cid) => (),
                    Ok(f) if f.is_valid() => count += 1,
                    _ => return false,
                }
            }

            count == 7
        })
        .count()
}
