use crate::*;

#[derive(Debug, Input, Clone)]
#[input(regex = r"^(\w+) \+?(-?\d+)$")]
pub struct Data<'s> {
    op: &'s str,
    amount: isize,
}

pub fn run(tape: &[Data]) -> (isize, isize) {
    let mut visited = vec![false; tape.len()];
    let mut ic = 0;
    let mut acc = 0;
    while let Some(i) = tape.get(ic as usize) {
        if replace(&mut visited[ic as usize], true) {
            break;
        };

        match i.op {
            "nop" => (),
            "acc" => acc += i.amount,
            "jmp" => ic += i.amount - 1,
            _ => panic!("unknown opcode {}", i.op),
        }

        ic += 1;
    }

    (ic, acc)
}

pub fn part1(tape: Vec<Data>) -> isize {
    run(&tape).1
}

pub fn part2(tape: Vec<Data>) -> isize {
    for idx in 0..tape.len() {
        let mut tape = tape.clone();
        let to_fix = &mut tape[idx];
        to_fix.op = match to_fix.op {
            "nop" => "jmp",
            "jmp" => "nop",
            _ => continue,
        };

        let (ic, acc) = run(&tape);
        if ic == tape.len() as isize {
            return acc;
        }
    }

    panic!("could not fix")
}
