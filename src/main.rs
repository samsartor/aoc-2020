#![feature(
    str_split_once,
    never_type,
    min_const_generics,
    array_map,
    array_windows,
    or_patterns
)]

use anyhow::{bail, Error};
pub use aoc_2020_derive::Input;
use std::{
    fs::{read_to_string, write, File},
    ops::RangeInclusive,
};
use std::{
    io::{stdin, Read},
    time::Instant,
};

mod grid;
mod input;
mod keyed;
mod split;
mod tools;

pub use crate::grid::*;
pub use crate::input::*;
pub use crate::keyed::*;
pub use crate::split::*;
pub use crate::tools::*;

#[derive(argh::FromArgs)]
/// Solve advent of code 2020.
struct AdventOfCode {
    /// the day number
    #[argh(positional)]
    day: Option<usize>,

    /// the part
    #[argh(option, short = 'p')]
    part: Option<usize>,

    /// manual input on stdin
    #[argh(switch, short = 'm')]
    manual: bool,

    /// example input
    #[argh(switch, short = 'e')]
    example: bool,

    /// test that the output is unchanged
    #[argh(switch, short = 't')]
    test: bool,
}

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;

aoc_2020_derive::dispatch_days!(15);

fn run_part(
    input: String,
    day: usize,
    parts: RangeInclusive<usize>,
    store: bool,
    test: bool,
    passed: &mut bool,
) -> Result<(), Error> {
    for part in parts {
        let start = Instant::now();
        let output = dispatch_day(day, part, input.trim());
        let elapsed = start.elapsed();
        match output {
            Ok(output) => {
                println!("OUTPUT (day {}, part {}): {}", day, part, &output);
                println!("\tDone in {}", humantime::format_duration(elapsed));
                let path = format!("outputs/day{:02}_{}.txt", day, part);
                if test {
                    match &read_to_string(path) {
                        Ok(existing) if existing == &output => {
                            println!("\tTEST PASSED");
                        }
                        Ok(existing) => {
                            println!("\tExpected {:?}", existing);
                            *passed = false;
                        }
                        Err(e) => {
                            println!("\tCould not read input: {}", e);
                            *passed = false;
                        }
                    }
                } else if store {
                    write(path, output)?;
                }
            }
            Err((found, expected)) => {
                let (row, col) = substr_row_col(&input, found);
                println!(
                    "INPUT ERROR in day {} (line {}, col {}): expected {} but found {:?}",
                    day,
                    row + 1,
                    col + 1,
                    expected,
                    found
                );
            }
        }
    }

    Ok(())
}

fn main() -> Result<(), Error> {
    let args = argh::from_env::<AdventOfCode>();
    let day = args.day.unwrap_or(DISPATCH_DAYS);
    let mut input = String::new();
    if args.manual {
        stdin().read_to_string(&mut input)?;
    } else if args.example {
        match File::open(format!("inputs/day{:02}_example.txt", day)) {
            Ok(mut f) => {
                f.read_to_string(&mut input)?;
            }
            Err(e) => bail!("Could not open example input for day {}: {}", day, e),
        }
    } else {
        match File::open(format!("inputs/day{:02}.txt", day)) {
            Ok(mut f) => {
                f.read_to_string(&mut input)?;
            }
            Err(e) => bail!("Could not open input for day {}: {}", day, e),
        }
    }

    let parts = match args.part {
        Some(p) => p..=p,
        None => 1..=2,
    };

    let mut tests_passed = true;
    run_part(
        input,
        day,
        parts,
        !args.manual && !args.example,
        args.test,
        &mut tests_passed,
    )?;
    if !tests_passed {
        bail!("TEST FAILED")
    }

    Ok(())
}

#[test]
pub fn test_unchanged_solutions() {
    let mut passed = true;

    let _block_stderr = gag::Gag::stderr();
    for day in 1..=DISPATCH_DAYS {
        if let Ok(input) = read_to_string(format!("inputs/day{:02}.txt", day)) {
            run_part(input, day, 1..=2, false, true, &mut passed).unwrap();
        }
    }

    assert!(passed);
}
