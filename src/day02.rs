use crate::*;

#[derive(Debug, Input)]
#[input(regex = r"^(\d+)-(\d+) (.): (.*)$")]
pub struct Password<'s> {
    first: usize,
    second: usize,
    required: char,
    password: &'s str,
}

impl<'s> Password<'s> {
    fn is_legal_1(&self) -> bool {
        let n = self.password.chars().count_eq(self.required);
        (self.first..=self.second).contains(&n)
    }

    fn is_legal_2(&self) -> bool {
        let a = self.password.at(self.first - 1);
        let b = self.password.at(self.second - 1);
        (a == self.required) != (b == self.required)
    }
}

pub fn part1(input: Vec<Password>) -> usize {
    input.into_iter().filter(Password::is_legal_1).count()
}

pub fn part2(input: Vec<Password>) -> usize {
    input.into_iter().filter(Password::is_legal_2).count()
}
