use crate::*;

pub fn part1(input: Vec<usize>) -> usize {
    for window in input.windows(26) {
        let n = window[25];
        if window[..25].combs().find(|&[&x, &y]| x + y == n).is_none() {
            return n;
        }
    }

    panic!()
}

pub fn part2(input: Vec<usize>) -> usize {
    let ans = part1(input.clone());

    let mut i = 0;
    let mut j = 0;
    let mut sum = 0;

    while j < input.len() {
        if sum == ans {
            let (a, b) = input[i..=j]
                .iter()
                .fold((usize::MIN, usize::MAX), |(a, b), &n| (a.max(n), b.min(n)));
            return a + b;
        }

        if sum < ans {
            sum += input[j];
            j += 1;
        } else {
            sum -= input[i];
            i += 1;
        }
    }

    panic!()
}
