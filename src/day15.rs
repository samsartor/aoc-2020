use crate::*;

fn play_game(mut input: Vec<usize>, end: usize) -> usize {
    let first_turn = input.len() + 1;
    let mut last = input.pop().unwrap();
    let mut when: M<_, _> = input.iter().enumerate().map(|(i, &x)| (x, i + 1)).collect();
    for n in first_turn..=end {
        let speek = match when.get(&last) {
            None => 0,
            Some(when) => n - *when - 1,
        };
        when.insert(last, n - 1);
        last = speek;
    }

    last
}

pub fn part1(input: SplitC<usize, ','>) -> usize {
    play_game(input.into(), 2020)
}

pub fn part2(input: SplitC<usize, ','>) -> usize {
    play_game(input.into(), 30000000)
}
