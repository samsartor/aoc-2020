use crate::*;

#[derive(Debug, Input)]
#[input(regex = r"^(.)(\d+)$")]
pub struct Instruction(char, isize);

fn rotate(angle: isize, (x, y): (isize, isize)) -> (isize, isize) {
    match angle {
        0 => (x, y),
        90 => (-y, x),
        180 => (-x, -y),
        270 => (y, -x),
        _ => panic!(),
    }
}

pub fn part1(input: Vec<Instruction>) -> isize {
    let mut d = (1, 0);
    let mut s = (0, 0);

    for Instruction(o, n) in input {
        match o {
            'E' => s.0 += n,
            'S' => s.1 += n,
            'W' => s.0 -= n,
            'N' => s.1 -= n,
            'R' => d = rotate(n, d),
            'L' => d = rotate(360 - n, d),
            'F' => s = (s.0 + d.0 * n, s.1 + d.1 * n),
            _ => panic!(),
        }
    }

    s.0.abs() + s.1.abs()
}

pub fn part2(input: Vec<Instruction>) -> isize {
    let mut w = (10, -1);
    let mut s = (0, 0);

    for Instruction(o, n) in input {
        match o {
            'E' => w.0 += n,
            'S' => w.1 += n,
            'W' => w.0 -= n,
            'N' => w.1 -= n,
            'R' => w = rotate(n, w),
            'L' => w = rotate(360 - n, w),
            'F' => s = (s.0 + w.0 * n, s.1 + w.1 * n),
            _ => panic!(),
        }
    }

    s.0.abs() + s.1.abs()
}
