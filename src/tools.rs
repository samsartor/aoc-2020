use crate::Input;
use std::hash::Hash;
use std::{collections::*, ops::Index};

pub type I = usize;
pub type V<T> = Vec<T>;
pub type S<T> = HashSet<T>;
pub type M<K, V> = HashMap<K, V>;

pub use std::collections::hash_map::Entry;
pub use std::mem::{replace, swap};

pub trait StrExt {
    fn read<'s, T: Input<'s>>(&'s self) -> T;

    fn at(&self, idx: usize) -> char;
}

impl StrExt for str {
    fn read<'s, T: Input<'s>>(&'s self) -> T {
        crate::unwrap_input_res(T::read(self))
    }

    fn at(&self, idx: usize) -> char {
        self[idx..].chars().next().expect("indexed end of string")
    }
}

pub trait IntExt {
    fn set_bit(self, idx: usize, value: bool) -> Self;
    fn get_bit(self, idx: usize) -> bool;
}

impl IntExt for usize {
    fn set_bit(self, idx: usize, value: bool) -> Self {
        let shifted = 1 << idx;
        match value {
            true => self | shifted,
            false => self & !shifted,
        }
    }

    fn get_bit(self, idx: usize) -> bool {
        (self >> idx) & 1 != 0
    }
}

pub trait MapExt<K, V> {
    fn mutate(&mut self, key: K) -> &mut V;
}

impl<K: Hash + Eq, V: Default> MapExt<K, V> for HashMap<K, V> {
    fn mutate(&mut self, key: K) -> &mut V {
        self.entry(key).or_default()
    }
}

impl<K: Ord, V: Default> MapExt<K, V> for BTreeMap<K, V> {
    fn mutate(&mut self, key: K) -> &mut V {
        self.entry(key).or_default()
    }
}

pub trait IterExt<I> {
    fn count_eq(self, value: I) -> usize
    where
        I: PartialEq;
}

impl<T: IntoIterator> IterExt<T::Item> for T {
    fn count_eq(self, value: T::Item) -> usize
    where
        T::Item: PartialEq,
    {
        self.into_iter().filter(eq(value)).count()
    }
}

pub trait SliceExt<'s, T> {
    fn combs<const K: usize>(self) -> Combs<'s, T, K>;

    fn replaced_combs<const K: usize>(self) -> RCombsConst<'s, T, K>;

    fn replaced_combs_with_k(self, k: usize) -> RCombs<'s, T>;
}

impl<'s, T> SliceExt<'s, T> for &'s [T] {
    /// Iterate over all combitations of `K` elemnts of self, *without
    /// replacement*. `K` is a constant.
    fn combs<const K: usize>(self) -> Combs<'s, T, K> {
        Combs::new(self.as_ref())
    }

    /// Iterate over all combitations of `K` elemnts of self, *with
    /// replacement*. `K` is a constant.
    fn replaced_combs<const K: usize>(self) -> RCombsConst<'s, T, K> {
        RCombsConst::new(self.as_ref())
    }

    /// Iterate over all combitations of `k` elemnts of self, *with
    /// replacement*. `k` is an argument.
    fn replaced_combs_with_k(self, k: usize) -> RCombs<'s, T> {
        RCombs::new(self.as_ref(), k)
    }
}

pub struct EnumerateCombs<const K: usize> {
    first: bool,
    n: usize,
    inds: [usize; K],
}

impl<const K: usize> EnumerateCombs<K> {
    pub fn new(n: usize) -> Self {
        assert!(n >= K);
        let mut inds = [0; K];
        for i in 0..K {
            inds[i] = i;
        }

        EnumerateCombs {
            inds,
            n,
            first: true,
        }
    }
}

impl<const K: usize> Iterator for EnumerateCombs<K> {
    type Item = [usize; K];

    fn next(&mut self) -> Option<Self::Item> {
        if replace(&mut self.first, false) {
            return Some(self.inds);
        }

        let mut to_inc = None;
        for i in (0..K).rev() {
            if self.inds[i] < self.n - K + i {
                to_inc = Some(i);
                break;
            }
        }

        let to_inc = to_inc?;
        let x = self.inds[to_inc];
        for i in to_inc..K {
            self.inds[i] = x - to_inc + i + 1;
        }

        Some(self.inds)
    }
}

#[test]
fn test_combs() {
    assert_eq!(
        EnumerateCombs::<3>::new(5).collect::<Vec<_>>(),
        vec![
            [0, 1, 2],
            [0, 1, 3],
            [0, 1, 4],
            [0, 2, 3],
            [0, 2, 4],
            [0, 3, 4],
            [1, 2, 3],
            [1, 2, 4],
            [1, 3, 4],
            [2, 3, 4],
        ]
    )
}

pub struct Combs<'l, T, const K: usize> {
    list: &'l [T],
    inds: EnumerateCombs<K>,
}

impl<'l, T, const K: usize> Combs<'l, T, K> {
    pub fn new(list: &'l [T]) -> Self {
        Combs {
            list,
            inds: EnumerateCombs::new(list.len()),
        }
    }
}

impl<'l, T, const N: usize> Iterator for Combs<'l, T, N> {
    type Item = [&'l T; N];

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.inds.next()?.map(|i| &self.list[i]))
    }
}

#[derive(Clone)]
pub struct RCombs<'l, T> {
    list: &'l [T],
    k: usize,
    step: usize,
}

impl<'l, T> RCombs<'l, T> {
    pub fn new(list: &'l [T], k: usize) -> Self {
        Self {
            list,
            k,
            step: list.len().pow(k as u32),
        }
    }
}

impl<'l, T> Iterator for RCombs<'l, T> {
    type Item = RCombList<'l, T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.step == 0 {
            return None;
        }
        self.step -= 1;

        let out = RCombList {
            list: &self.list,
            k: self.k,
            step: self.step,
        };

        Some(out)
    }
}

#[derive(Clone)]
pub struct RCombList<'l, T> {
    list: &'l [T],
    k: usize,
    step: usize,
}

impl<'l, T> Index<usize> for RCombList<'l, T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        assert!(index < self.k);
        let i = (self.step / self.list.len().pow(index as u32)) % self.list.len();
        &self.list[i]
    }
}

impl<'l, T> Iterator for RCombList<'l, T> {
    type Item = &'l T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.k == 0 {
            return None;
        }

        let out = &self.list[self.step % self.list.len()];
        self.k -= 1;
        self.step /= self.list.len();

        Some(out)
    }
}

pub struct RCombsConst<'l, T, const K: usize> {
    combs: RCombs<'l, T>,
}

impl<'l, T, const K: usize> RCombsConst<'l, T, K> {
    pub fn new(list: &'l [T]) -> Self {
        RCombsConst {
            combs: RCombs::new(list, K),
        }
    }
}

impl<'l, T, const K: usize> Iterator for RCombsConst<'l, T, K> {
    type Item = [&'l T; K];

    fn next(&mut self) -> Option<Self::Item> {
        let mut list = self.combs.next()?;
        let arr = [(); K];
        Some(arr.map(|()| list.next().unwrap()))
    }
}

pub fn eq<T: PartialEq>(value: T) -> impl Fn(&T) -> bool {
    move |x| value.eq(x)
}
