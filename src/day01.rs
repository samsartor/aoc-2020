use crate::SliceExt as _;

pub fn part1(nums: Vec<u32>) -> u32 {
    for [&a, &b] in nums.combs() {
        if a + b == 2020 {
            return a * b;
        }
    }

    panic!("no solution")
}

pub fn part2(nums: Vec<u32>) -> u32 {
    for [&a, &b, &c] in nums.combs() {
        if a + b + c == 2020 {
            return a * b * c;
        }
    }

    panic!("no solution")
}
