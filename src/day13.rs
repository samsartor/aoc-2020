use crate::*;

#[derive(Debug, Input, Copy, Clone)]
#[input(split = r",")]
pub enum Bus {
    #[input(regex = r"^(\d+)$")]
    Id(usize),
    #[input(regex = r"^x$")]
    Disabled,
}

impl Bus {
    fn id(self) -> Option<usize> {
        match self {
            Bus::Id(i) => Some(i),
            _ => None,
        }
    }
}

#[derive(Debug, Input)]
#[input(regex = r"^(\d+)\n(.*)$")]
pub struct Schedule {
    earliest: usize,
    buses: Split<Bus>,
}

impl Schedule {
    fn ids(&self) -> impl Iterator<Item = (usize, usize)> + '_ {
        self.buses
            .iter()
            .copied()
            .enumerate()
            .filter_map(|(i, b)| Some((i, b.id()?)))
    }
}

pub fn part1(input: Schedule) -> usize {
    let (_, found) = input
        .ids()
        .min_by_key(|&(_, i)| i - input.earliest % i)
        .unwrap();
    found * (found - input.earliest % found)
}

pub fn part2(input: Schedule) -> usize {
    let mut lcm = 1;
    let mut t = 0;
    for (later, id) in input.ids() {
        // Note that `t` has the correct remainder for all busess prior to this
        // one and also that `lcm` can be added to `t` without changing any of
        // those remainders. So, just keep doing `t += lcm` until we find a new
        // `t` that is valid for this bus as well.
        while (t + later) % id != 0 {
            t += lcm;
        }

        // Bus ids are all prime, so we can get the next LCM just with:
        lcm *= id;

        // Just to make debugging easier.
        debug_assert!(t < lcm);
        eprintln!("{}+{}={}k", t, later, id);
    }

    t
}
