use crate::Grid;

fn count(grid: &Grid, dx: usize, dy: usize) -> usize {
    let mut x = 0;
    let mut y = 0;
    let mut count = 0;
    while y < grid.h {
        if grid.atu(x, y) == '#' {
            count += 1;
        }

        x = (x + dx) % grid.w;
        y = y + dy;
    }

    count
}

pub fn part1(grid: Grid) -> usize {
    count(&grid, 3, 1)
}

pub fn part2(grid: Grid) -> usize {
    let mut acc = count(&grid, 1, 2);
    for &i in &[1, 3, 5, 7] {
        acc *= count(&grid, i, 1)
    }
    acc
}
