use crate::*;

#[derive(Debug, Input)]
pub enum Instruction {
    #[input(regex = r"^mask = ([X\d]+)$")]
    Mask(String),
    #[input(regex = r"^mem\[(\d+)\] = (\d+)$")]
    Write(usize, usize),
}

use Instruction::*;

pub fn part1(input: Vec<Instruction>) -> usize {
    let mut mem = M::new();
    let mut and = !0;
    let mut or = 0;

    for ins in input {
        match ins {
            Mask(mask) => {
                and = !0;
                or = 0;
                for (i, m) in mask.chars().rev().enumerate() {
                    match m {
                        '1' => or = or.set_bit(i, true),
                        '0' => and = and.set_bit(i, false),
                        _ => (),
                    }
                }
            }
            Write(addr, val) => {
                mem.insert(addr, (val & and) | or);
            }
        }
    }

    mem.values().sum()
}

pub fn part2(input: Vec<Instruction>) -> usize {
    let mut mem = M::new();
    let mut mask = String::new();

    for ins in input {
        match ins {
            Write(mut addr, val) => {
                let k = mask.chars().count_eq('X');
                for mut floating in [false, true].replaced_combs_with_k(k) {
                    for (i, m) in mask.chars().rev().enumerate() {
                        let bit = match m {
                            '0' => continue,
                            '1' => true,
                            'X' => *floating.next().unwrap(),
                            _ => panic!(),
                        };
                        addr = addr.set_bit(i, bit);
                    }
                    mem.insert(addr, val);
                }
            }
            Mask(new_mask) => {
                mask = new_mask;
            }
        }
    }

    mem.values().sum()
}
