use std::collections::BTreeMap;

use crate::Groups;

fn count_questions(group: &str, minimum: usize) -> usize {
    let mut qs: BTreeMap<_, usize> = Default::default();
    for c in group.chars() {
        if c.is_alphabetic() {
            *qs.entry(c).or_insert(0) += 1;
        }
    }
    qs.values().filter(|n| **n >= minimum).count()
}

pub fn part1(input: Groups<&str>) -> usize {
    input.into_iter().map(|g| count_questions(&*g, 0)).sum()
}

pub fn part2(input: Groups<&str>) -> usize {
    input
        .into_iter()
        .map(|group| {
            let num_people = group.lines().count();
            count_questions(&*group, num_people)
        })
        .sum()
}
