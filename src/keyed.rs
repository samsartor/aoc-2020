use crate::{Delimited, Input, InputRes};
use std::collections::{BTreeMap, HashMap};
use std::hash::Hash;

pub trait Keyed<'s> {
    type Key: Clone + 's;

    fn key(&self) -> &Self::Key;
}

impl<'s, T: Input<'s> + Keyed<'s> + Delimited> Input<'s> for HashMap<T::Key, T>
where
    T::Key: Hash + Eq,
{
    fn read(input: &'s str) -> InputRes<'s, Self> {
        let mut map = HashMap::new();
        for pair in T::PATTERN.split(input) {
            let item = T::read(pair)?;
            map.insert(item.key().clone(), item);
        }
        Ok(map)
    }
}

impl<'s, T: Input<'s> + Keyed<'s> + Delimited> Input<'s> for BTreeMap<T::Key, T>
where
    T::Key: Ord,
{
    fn read(input: &'s str) -> InputRes<'s, Self> {
        let mut map = BTreeMap::new();
        for pair in T::PATTERN.split(input) {
            let item = T::read(pair)?;
            map.insert(item.key().clone(), item);
        }
        Ok(map)
    }
}
