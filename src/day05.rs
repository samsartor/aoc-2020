#[rustfmt::skip]
fn to_id(pass: &str) -> u32 {
    pass.chars().fold(0, |i, c| (i << 1) | match c {
        'F' | 'L' => 0,
        'B' | 'R' => 1,
        _ => panic!(),
    })
}

pub fn part2(input: Vec<&str>) -> u32 {
    let mut ids: Vec<_> = input.into_iter().map(to_id).collect();
    ids.sort();

    let mut last = None;
    for id in ids {
        if let Some(last) = last {
            if last + 2 == id {
                return last + 1;
            }
        }
        last = Some(id);
    }

    panic!()
}

pub fn part1(input: Vec<&str>) -> u32 {
    input.into_iter().map(to_id).max().unwrap()
}
