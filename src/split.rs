use crate::{Input, InputRes};
use once_cell::sync::Lazy;
use regex::Regex;
use std::ops::{Deref, DerefMut};

pub trait Delimited {
    const PATTERN: Lazy<Regex>;
}

#[derive(Debug)]
pub struct Split<T>(pub Vec<T>);

impl<T> Into<Vec<T>> for Split<T> {
    fn into(self) -> Vec<T> {
        self.0
    }
}

impl<T> IntoIterator for Split<T> {
    type Item = T;

    type IntoIter = std::vec::IntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<T> Deref for Split<T> {
    type Target = Vec<T>;

    fn deref(&self) -> &Vec<T> {
        &self.0
    }
}

impl<T> DerefMut for Split<T> {
    fn deref_mut(&mut self) -> &mut Vec<T> {
        &mut self.0
    }
}

impl<'s, T: Input<'s> + Delimited> Input<'s> for Split<T> {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        Ok(Split(
            T::PATTERN
                .split(input)
                .map(|s| T::read(s))
                .collect::<Result<_, _>>()?,
        ))
    }
}

#[derive(Debug)]
pub struct SplitC<T, const C: char>(pub Vec<T>);

impl<T, const C: char> Into<Vec<T>> for SplitC<T, C> {
    fn into(self) -> Vec<T> {
        self.0
    }
}

impl<T, const C: char> IntoIterator for SplitC<T, C> {
    type Item = T;

    type IntoIter = std::vec::IntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<T, const C: char> Deref for SplitC<T, C> {
    type Target = Vec<T>;

    fn deref(&self) -> &Vec<T> {
        &self.0
    }
}

impl<T, const C: char> DerefMut for SplitC<T, C> {
    fn deref_mut(&mut self) -> &mut Vec<T> {
        &mut self.0
    }
}

impl<'s, T: Input<'s>, const C: char> Input<'s> for SplitC<T, C> {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        Ok(SplitC(
            input
                .split(C)
                .map(|s| T::read(s))
                .collect::<Result<_, _>>()?,
        ))
    }
}

#[derive(Debug)]
pub struct Group<T>(pub T);

impl<T> Deref for Group<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

impl<T> DerefMut for Group<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.0
    }
}

impl<T> Delimited for Group<T> {
    const PATTERN: Lazy<Regex> = Lazy::new(|| Regex::new("\n\n").unwrap());
}

impl<'s, T: Delimited> Delimited for InputRes<'s, T> {
    const PATTERN: Lazy<Regex> = T::PATTERN;
}

impl<'s, T: Input<'s>> Input<'s> for Group<T> {
    fn read(input: &'s str) -> InputRes<'s, Self> {
        Ok(Group(T::read(input)?))
    }
}

pub type Groups<T> = Split<Group<T>>;
