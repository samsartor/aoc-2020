#![feature(option_expect_none)]

extern crate proc_macro;

use darling::{ast::Data, FromDeriveInput, FromField, FromVariant};
use proc_macro::TokenStream;
use proc_macro2::TokenStream as TokenStream2;
use quote::{format_ident, quote};
use syn::{parse_macro_input, parse_quote, DeriveInput};

#[proc_macro]
pub fn dispatch_days(input: TokenStream) -> TokenStream {
    let days_n: usize = parse_macro_input!(input as syn::LitInt)
        .base10_parse()
        .unwrap();
    let mut lines = Vec::new();
    for day_i in 1..=days_n {
        let day_m = format_ident!("day{:02}", day_i);

        for part_i in 1usize..=2 {
            let part_f = format_ident!("part{}", part_i);

            lines.push(quote! {
                (#day_i, #part_i) => Ok(#day_m::#part_f(crate::Input::read(input)?).to_string())
            });
        }
    }

    (quote! {
        const DISPATCH_DAYS: usize = #days_n;

        fn dispatch_day<'s>(day: usize, part: usize, input: &'s str) -> crate::InputRes<'s, String> {
            match (day, part) {
                #(#lines,)*
                _ => todo!()
            }
        }
    })
    .into()
}

#[derive(FromField)]
struct InputField {
    ty: syn::Type,
    ident: Option<syn::Ident>,
}

#[derive(FromVariant)]
#[darling(attributes(input))]
struct InputVariant {
    ident: syn::Ident,
    fields: darling::ast::Fields<InputField>,
    regex: String,
}

#[derive(FromDeriveInput)]
#[darling(attributes(input))]
struct InputOpts {
    ident: syn::Ident,
    generics: syn::Generics,
    data: Data<InputVariant, InputField>,
    #[darling(default)]
    regex: Option<String>,
    #[darling(default)]
    split: Option<String>,
    #[darling(default)]
    keyed: Option<syn::Ident>,
}

fn input_func(
    func: &syn::Ident,
    out: &syn::Ident,
    construct: &syn::Path,
    regex: &str,
    fields: Vec<InputField>,
) -> TokenStream2 {
    enum StructForm {
        Braces,
        Parens,
        Unit,
    }

    use StructForm::*;

    let form = fields
        .iter()
        .next()
        .map(|f| if f.ident.is_some() { Braces } else { Parens })
        .unwrap_or(Unit);

    let parsers = fields.iter().enumerate().map(|(i, f)| {
        let group_err = format!("Group ${} does not exist in /{}/.", i, regex);
        let missing_err = format!("/{}/ with ${}", regex, i);

        let next_str = quote!(match captures.next() {
            Some(Some(cap)) => {
                n += 1;
                cap.as_str()
            }
            Some(None) => return Err((input, #missing_err)),
            None => panic!("{}", #group_err),
        });

        let mut parser = if let syn::Type::Reference(_) = f.ty {
            next_str.clone()
        } else {
            quote!(crate::Input::read(#next_str)?)
        };

        if let syn::Type::Path(p) = &f.ty {
            if p.path
                .segments
                .first()
                .map(|s| s.ident == "Option")
                .unwrap_or(false)
            {
                parser = quote!(match captures.next() {
                    Some(Some(cap)) => {
                        n += 1;
                        Some(crate::Input::read(cap.as_str())?)
                    }
                    Some(None) => None,
                    None => panic!("{}", #group_err),
                });
            }
        }

        match &f.ident {
            Some(ident) => quote!(#ident: #parser),
            None => parser,
        }
    });

    let output = match form {
        Braces => quote! {
            #construct {
                #(#parsers),*
            }
        },
        Parens => quote! {
            #construct(
                #(#parsers),*
            )
        },
        Unit => quote! { #construct },
    };

    let match_err = format!("/{}/", regex);
    quote! {
        fn #func<'i>(input: &'i str) -> crate::InputRes<'i, #out> {
            static REGEX: ::once_cell::sync::Lazy<::regex::Regex> = ::once_cell::sync::Lazy::new(|| {
                ::regex::RegexBuilder::new(#regex).dot_matches_new_line(true).build().unwrap()
            });

            let captures = match REGEX.captures(input) {
                Some(cap) => cap,
                None => return Err((input, #match_err)),
            };
            let mut captures = captures.iter().skip(1);
            let mut n = 0;

            Ok(#output)
        }
    }
}

#[proc_macro_derive(Input, attributes(input))]
pub fn easy_input(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let opts: InputOpts = InputOpts::from_derive_input(&input).unwrap();

    let ident = opts.ident;

    let (imp_a, ty, wher) = opts.generics.split_for_impl();
    let mut generics = opts.generics.clone();
    let lt = generics.lifetimes().next().cloned();
    let lt = lt.unwrap_or_else(|| {
        let lt: syn::LifetimeDef = parse_quote!('s);
        generics.params.push(lt.clone().into());
        lt
    });
    let (imp_b, _, _) = generics.split_for_impl();

    let mut output = quote! {};

    match opts.data {
        Data::Enum(dat) => {
            opts.regex
                .expect_none("regex must be specified for individual variants");

            let mut tries = Vec::new();
            let num = dat.len();

            for (i, var) in dat.into_iter().enumerate() {
                let inner = format_ident!("read_{}_", i);
                let var_i = var.ident;
                let inner_impl = input_func(
                    &inner,
                    &ident,
                    &parse_quote!(#ident::#var_i),
                    &var.regex,
                    var.fields.fields,
                );
                tries.push(quote! {
                    #inner_impl

                    match #inner(input) {
                        Ok(done) => return Ok(done),
                        Err(x) => errs[#i] = x,
                    }
                });
            }

            let non_err = format!("<one of {}>", ident);
            output.extend(quote! {
                impl #imp_b crate::Input<#lt> for #ident #ty #wher {
                    fn read(input: &#lt str) -> crate::InputRes<#lt, Self> {
                        let mut errs = [("", ""); #num];

                        #(#tries)*

                        eprintln!("WARNING matched none:");
                        for &(found, expected) in &errs {
                            eprintln!("\texpected {} but found {:?}.", expected, found);
                        }

                        Err((input, #non_err))
                    }
                }
            });
        }
        Data::Struct(dat) => {
            if let Some(keyed) = opts.keyed {
                let key_ty = &dat
                    .fields
                    .iter()
                    .find(|f| match &f.ident {
                        Some(i) => i == &keyed,
                        None => false,
                    })
                    .expect(&format!("unknown field {}", keyed))
                    .ty;

                output.extend(quote! {
                    impl #imp_b crate::Keyed<#lt> for #ident #ty #wher {
                        type Key = #key_ty;

                        fn key(&self) -> &Self::Key {
                            &self.#keyed
                        }
                    }
                });
            }

            let regex = opts.regex.expect("regex not specified");
            let inner_func = input_func(
                &format_ident!("read_"),
                &ident,
                &ident.clone().into(),
                &regex,
                dat.fields,
            );
            output.extend(quote! {
                impl #imp_b crate::Input<#lt> for #ident #ty #wher {
                    fn read(input: &#lt str) -> crate::InputRes<#lt, Self> {
                        #inner_func

                        read_(input)
                    }
                }
            });
        }
    }

    if let Some(split) = opts.split {
        output.extend(quote! {
            impl #imp_a crate::Delimited for #ident #ty #wher {
                const PATTERN: ::once_cell::sync::Lazy<::regex::Regex> = ::once_cell::sync::Lazy::new(|| ::regex::Regex::new(#split).unwrap());
            }
        });
    }

    output.into()
}
